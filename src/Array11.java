import java.util.Scanner;

public class Array11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[5];
        int first, second;
        for (int i =0; i < arr.length; i++){
            arr[i] = (int)(Math.random()*100);
         }
        while(true){
            for (int i = 0; i < arr.length; i++) {
                System.out.print(arr[i] + " ");
            }
            System.out.println();
    
            System.out.print("Please input index: ");
            first = sc.nextInt();
            second = sc.nextInt();
    
            int temp = arr[first];
            arr[first] = arr[second];
            arr[second] = temp;

            boolean isFinish = true;
            for (int i = 1; i < arr.length; i++){
                if(arr[i-1]>arr[i]){
                    isFinish = false;
                }
            }
            if(isFinish){
                for (int i = 0; i < arr.length; i++) {
                    System.out.print(arr[i] + " ");
                }
                System.out.println();
                System.out.println("You win!!!");
                break;
            }
        } 
    }
}
