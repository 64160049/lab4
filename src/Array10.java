import java.util.Scanner;

public class Array10 {
    public static void main(String[] args) {
        int size = 0;
        int arr[];
        int uniqeArr[];
        int uniSize = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number of element: ");
        size = sc.nextInt();
        arr = new int[size];
        uniqeArr = new int[size];
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Element " + i + ": ");
            arr[i] = sc.nextInt();
            int index = -1;
            for (int j = 0; j < uniSize; j++) {
                if (uniqeArr[j] == arr[i]) {
                    index = j;
                }
            }
            if (index < 0) {
                uniqeArr[uniSize] = arr[i];
                uniSize++;
            }
        }
        System.out.print("arr = ");
        for (int i = 0; i < uniSize; i++) {
            System.out.print(uniqeArr[i] + " ");

        }
    }
}
