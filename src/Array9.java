import java.util.Scanner;

public class Array9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        int arr [] = new int[3];
        for (int i=0;i<arr.length;i++){
            System.out.print("Please input arr[" + i + "]:");
            arr[i] = sc.nextInt();

        }
        System.out.print("arr = ");
        for(int i =0;i<arr.length;i++){
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        System.out.println("Please input search value");
        int searchValue = sc.nextInt();
        int index = -1;
        for(int i =0; i <arr.length;i++){
            if(arr[i] == searchValue){
                index = i;
                break;

            }
         }
         if(index >=0){
             System.out.println("found at index: " + index);
         }else{
             System.out.println("not found");
         }
    }
}
