import java.util.Scanner;

public class Array7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        int arr [] = new int[3];
        for (int i=0;i<arr.length;i++){
            System.out.print("Please input arr[" + i + "]:");
            arr[i] = sc.nextInt();

        }
        System.out.print("arr = ");
        for(int i =0;i<arr.length;i++){
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        int sum = 0;
        for(int i =0; i<arr.length;i++) {
            sum = sum + arr[i];
        }
        System.out.println("sum = " + sum);

        double avg = ((double)sum)/arr.length;
        System.out.println("avg = " + avg);

        int min = arr[0];
        for (int i=1; i<arr.length;i++){
            if(min>arr[i]) {
                min = arr[i];
            }
        }
        System.out.println("min = " + min);
    }
}